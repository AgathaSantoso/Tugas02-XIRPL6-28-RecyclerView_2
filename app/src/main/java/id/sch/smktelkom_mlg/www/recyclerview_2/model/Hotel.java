package id.sch.smktelkom_mlg.www.recyclerview_2.model;

import android.graphics.drawable.Drawable;

/**
 * Created by NAT on 16/02/2018.
 */

public class Hotel {
    public String judul, deskripsi;
    public Drawable foto;

    public Hotel(String judul, String deskripsi, Drawable foto) {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.foto = foto;
    }
}
